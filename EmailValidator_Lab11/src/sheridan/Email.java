package sheridan;

public class Email {

	// 1. Email must be in this format: <account>@<domain>.<extension>
	public boolean isValidEmail(String email) {

		String pattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		return email.matches(pattern);
	}

	// 2. Email should have one and only one @ symbol.
	public boolean isValidCharEmail(String email) {

		String pattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		return email.matches(pattern);
	}

	// 3. Account name should have at least 3 alpha-characters in lowercase (must
	// not start with a number)
	public boolean checkLowercaseCharseAndDigitAccountName(String email) {

		String account = email.substring(0, email.indexOf("@"));

		int count = 0;

		boolean checkCount = false;
		for (char c : account.toCharArray()) {
			if (Character.isLowerCase(c)) {
				count++;
			}
			if (count == 3) {
				checkCount = true;
				break;
			}
		}

		return checkCount && Character.isLetter(account.charAt(0));
	}

	// 4. Domain name should have at least 3 alpha-characters in lowercase or
	// numbers.
	public boolean checkDomainLower(String email) {

		String domainName = email.substring(email.indexOf("@") + 1, email.indexOf("."));

		int count = 0;

		boolean checkCount = false;

		for (char c : domainName.toCharArray()) {
			if (Character.isLowerCase(c) || Character.isDigit(c)) {
				count++;
			}
			if (count == 3) {
				checkCount = true;
				break;
			}
		}

		return checkCount;
	}

	// 5. Extension name should have at least 2 alpha-characters (no numbers)
	public boolean isValidCharExtension(String email) {

		String pattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,}$";

		return email.matches(pattern);
	}
}
