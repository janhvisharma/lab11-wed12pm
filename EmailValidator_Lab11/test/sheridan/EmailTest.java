package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailTest {

	// 1. Email must be in this format: <account>@<domain>.<extension>
	// Regular
	@Test
	public void testEmailFormatRegular() {
		Email emValid = new Email();
		boolean result = emValid.isValidEmail("janhvi@gmail.com");
		assertTrue(result);
	}

	// Exception
	@Test
	public void testEmailFormatException() {
		Email emValid = new Email();
		boolean result = emValid.isValidEmail("@gmail..com");
		assertFalse("Invalid Email Characters", result);
	}

	// Boundary-In
	@Test
	public void testEmailFormatBoundaryIn() {
		Email emValid = new Email();
		boolean result = emValid.isValidEmail("j@g.c");
		assertTrue(result);
	}

	// Boundary-Out
	@Test
	public void testEmailFormatBoundaryOut() {
		Email emValid = new Email();
		assertFalse("Invalid Email Characters", emValid.isValidEmail("janhvi.com"));
	}

	// 2. Email should have one and only one @ symbol.
	// Regular
	@Test
	public void testCharEmailRegular() {
		Email emValid = new Email();
		boolean result = emValid.isValidCharEmail("janhvi@gmail.com");
		assertTrue(result);
	}

	// Exception
	@Test
	public void testCharEmailException() {
		Email emValid = new Email();
		boolean result = emValid.isValidCharEmail("janhvi@@sharma.com");
		assertFalse("Invalid Number of @ Characters", result);
	}

	// 3. Account name should have at least 3 alpha-characters in lowercase (must
	// not start with a number)

	// Regular
	@Test
	public void testcheckLowercaseCharseAndDigitAccountNameRegular() {
		Email emValid = new Email();
		boolean result = emValid.checkLowercaseCharseAndDigitAccountName("janhvi@gmail.com");
		assertTrue(result);
	}

	// Exception
	@Test
	public void testcheckLowercaseCharseAndDigitAccountNameException() {
		Email emValid = new Email();
		boolean result = emValid.checkLowercaseCharseAndDigitAccountName("28j@gmail.com");
		assertFalse("Invalid Characters", result);
	}

	// Exception - Upper
	@Test
	public void testcheckLowercaseCharseAndDigitAccountNameExceptionUpper() {
		Email emValid = new Email();
		boolean result = emValid.checkLowercaseCharseAndDigitAccountName("JANHVI@gmail.com");
		assertFalse("Invalid Characters", result);
	}

	// Boundary-In
	@Test
	public void testcheckLowercaseCharseAndDigitAccountNameBoundaryIn() {
		Email emValid = new Email();
		boolean result = emValid.checkLowercaseCharseAndDigitAccountName("jans@gmail.com");
		assertTrue(result);
	}

	// Boundary-Out
	@Test
	public void testcheckLowercaseCharseAndDigitAccountNameBoundaryOut() {
		Email emValid = new Email();
		boolean result = emValid.checkLowercaseCharseAndDigitAccountName("j@gmail.com");
		assertFalse("Invalid Characters", result);
	}

	// 4. Domain name should have at least 3 alpha-characters in lowercase or
	// numbers.
	// Regular
	@Test
	public void testcheckDomainLowerRegular() {
		Email emValid = new Email();
		boolean result = emValid.checkDomainLower("js@xyz.com");
		assertTrue(result);
	}

	// Exception
	@Test
	public void testcheckDomainLowerCase() {
		Email emValid = new Email();
		boolean result = emValid.checkDomainLower("js@XYZZ.com");
		assertFalse("Invalid Characters", result);
	}

	// Boundary-In
	@Test
	public void testcheckDomainLowerBoundaryIn() {
		Email emValid = new Email();
		boolean result = emValid.checkDomainLower("js@xy2.com");
		assertTrue(result);
	}

	// Boundary-Out
	@Test
	public void testcheckDomainLowerBoundaryOut() {
		Email emValid = new Email();
		boolean result = emValid.checkDomainLower("js@js.com");
		assertFalse("Invalid Characters", result);
	}

	// 5. Extension name should have at least 2 alpha-characters (no numbers)
	// Regular
	@Test
	public void testCharExtensionRegular() {
		Email emValid = new Email();
		boolean result = emValid.isValidCharExtension("janhvi@sharma.com");
		assertTrue(result);
	}

	// Exception
	@Test
	public void testCharExtensionException() {
		Email emValid = new Email();
		boolean result = emValid.isValidCharExtension("janhvi@gmail.123");
		assertFalse("Invalid Email chars", result);
	}

	// Boundary-In
	@Test
	public void testCharExtensionBoundaryIn() {
		Email emValid = new Email();
		boolean result = emValid.isValidCharExtension("janhvi@gmail.co");
		assertTrue(result);
	}

	// Boundary-Out
	@Test
	public void testCharEmailBoundaryOut() {
		Email emValid = new Email();
		boolean result = emValid.isValidCharExtension("janhvi@gmail.c");
		assertFalse("Invalid Email chars", result);
	}
}
